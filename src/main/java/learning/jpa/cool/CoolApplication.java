package learning.jpa.cool;

import learning.jpa.cool.domain.Group;
import learning.jpa.cool.domain.Project;
import learning.jpa.cool.domain.Student;
import learning.jpa.cool.domain.StudentDetails;
import learning.jpa.cool.repositoy.GroupRepository;
import learning.jpa.cool.repositoy.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;


@SpringBootApplication
@RequiredArgsConstructor
public class CoolApplication implements CommandLineRunner {

    private final GroupRepository groupRepository;
    private final StudentRepository studentRepository;
    private final EntityManagerFactory entityManagerFactory;

    public static void main(String[] args) {
        SpringApplication.run(CoolApplication.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {


         create5Group3Student2Project();

        //======= GET DATA WITH ENTITY GRAPH ATTRIBUTES PATH  ==============
        getAllWithEntityGraphAttributesPath();


        //"======= GET DATA WITH NAMED ENTITY GRAPH  ==============
           getAllWithEntityGraph();
    }

    private void getAllWithEntityGraphAttributesPath() {
        List<Group> allWithEntityGraphAttributes = groupRepository.getAllWithEntityGraphAttributes();

        for (Group gr : allWithEntityGraphAttributes) {
            System.out.println(gr);
        }
    }

    private void getAllWithEntityGraph() {
        List<Group> allWithEntityGraph = groupRepository.getAllWithEntityGraph();

        for (Group gr : allWithEntityGraph) {
            System.out.println(gr);
        }
    }


    private void getAllWithNamedEntityGraphs(Long id) {

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        EntityGraph<?> entityGraph = entityManager.createEntityGraph("graph.group");

        Group group = entityManager.find(Group.class, id,
                Collections.singletonMap("javax.persistence.loadgraph", entityGraph));


        System.out.println(group);

        entityManager.close();
    }


    private void create5Group3Student2Project() {
        int sIndex = 1;
        int pIndex = 1;
        for (int g = 1; g <= 5; g++) {
            Group group = new Group("Group_" + g);

            for (int s = 1; s <= 3; s++) {
                Student student = new Student("Student_" + sIndex);

                StudentDetails studentDetails = new StudentDetails("Student_details_" + sIndex);

                student.setStudentDetailsBiDirectional(studentDetails);

                for (int p = 1; p <= 2; p++) {
                    Project project = new Project("Project_" + pIndex);
                    student.addProject(project);
                    pIndex++;
                }
                group.addStudent(student);
                sIndex++;
            }
            groupRepository.save(group);
        }
    }

    //no need ,just example
    private void createStudentAndProjects() {

        Student student1 = new Student("Neji");
        Student student2 = new Student("Shikamaru");

        Project project11 = new Project("Save konoha");
        Project project12 = new Project("Save team");

        Project project21 = new Project("Project 1");
        Project project22 = new Project("Project 2");

        student1.addProject(project11);
        student1.addProject(project12);

        student2.addProject(project21);
        student2.addProject(project22);

        studentRepository.save(student1);
        studentRepository.save(student2);
    }

    //no need ,just example
    private void createStudentAndGroup() {
        Group group1 = new Group("Group_1");
        Group group2 = new Group("Group_2");

        Student student11 = new Student("Naruto");
        Student student12 = new Student("Rock Lee");

        Student student21 = new Student("Itachi");
        Student student22 = new Student("Sasuke");

        group1.addStudent(student11);
        group1.addStudent(student12);
        group1.addStudent(student22);

        group2.addStudent(student11);
        group2.addStudent(student21);
        group2.addStudent(student22);

        groupRepository.save(group1);
        groupRepository.save(group2);
    }


}
