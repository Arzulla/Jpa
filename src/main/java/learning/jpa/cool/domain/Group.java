package learning.jpa.cool.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Data
@Entity(name = "S_Groups")
@Table(name = "s_groups")
@NamedEntityGraph(name = "graph.group",
        attributeNodes = {
                @NamedAttributeNode(value = "students", subgraph = "subgraph.students"),
        },
        subgraphs = {
                @NamedSubgraph(name = "subgraph.students",
                        attributeNodes = {
                                @NamedAttributeNode("projects"),
                                @NamedAttributeNode("studentDetails")
                        })
        }
)
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    public Group() {
    }

    public Group(String name) {
        this.name = name;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "group_student",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id"))
    private Set<Student> students = new HashSet<>();

    public void addStudent(Student student) {
        students.add(student);
        student.getGroups().add(this);
    }

    public void removeStudent(Student student) {
        students.remove(student);
        student.getGroups().remove(student);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Group)) return false;
        return id != null && id.equals(((Group) o).getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name);
    }
}
