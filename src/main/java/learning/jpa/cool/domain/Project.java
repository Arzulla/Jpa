package learning.jpa.cool.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.JoinFormula;

import javax.persistence.*;

@Data
@Entity(name = "Projects")
@Table(name = "projects")
@EqualsAndHashCode(exclude = "student")
@ToString(exclude = "student")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private Student student;

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }
}
