package learning.jpa.cool.domain;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.*;


@Data
@Entity(name = "Students")
@Table(name = "students")
@ToString(exclude = "groups")


public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "students")
    private Set<Group> groups = new HashSet<>();

    @OneToMany(mappedBy = "student",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Project> projects=new HashSet<>();

    @OneToOne(mappedBy = "student",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private StudentDetails studentDetails;

    public Student() {
    }

    public Student(String name) {
        this.name = name;
    }

    public  void addProject(Project project){
        projects.add(project);
        project.setStudent(this);
    }

    public  void removeProject(Project project){
        projects.remove(project);
        project.setStudent(null);
    }


    public void setStudentDetailsBiDirectional(StudentDetails studentDetails){
     setStudentDetails(studentDetails);
     studentDetails.setStudent(this);
    }

    @Override
    public  boolean equals(Object o){
        if(this == o) {
            return  true;
        }
        if(!(o instanceof Student)) return  false;
        return id != null && id.equals(((Student) o).getId());
    }

    @Override
    public  int hashCode(){
        return Objects.hashCode(name);
    }
}
