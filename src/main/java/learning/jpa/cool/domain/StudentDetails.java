package learning.jpa.cool.domain;


import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Data
@Entity(name = "Student_Details")
@Table(name = "student_details")
@ToString(exclude = "student")
public class StudentDetails {

    @Id
   @Column(name = "student_id")
    private Long id;

    private String name;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    @JoinColumn(name = "student_id")
    private Student student;

    public StudentDetails() {

    }

    public StudentDetails(String name) {
        this.name = name;
    }
}
