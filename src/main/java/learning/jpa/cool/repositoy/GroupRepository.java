package learning.jpa.cool.repositoy;

import learning.jpa.cool.domain.Group;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, Long> {

    @EntityGraph(attributePaths = {"students", "students.projects", "students.studentDetails"})
    @Query("SELECT g from S_Groups g  " +
            "where g.id=:groupId")
    Group getAllWithEntityGraphAttributes(@Param("groupId") Long id);

    @EntityGraph(attributePaths = {"students", "students.projects", "students.studentDetails"})
    @Query("SELECT g from S_Groups g ")
    List<Group> getAllWithEntityGraphAttributes();

    @EntityGraph(value = "graph.group", type = EntityGraph.EntityGraphType.LOAD)
    @Query("SELECT g from S_Groups  g")
    List<Group> getAllWithEntityGraph();


    @Query(nativeQuery = true, value = "SELECT  * FROM   s_groups g" +
            "       INNER JOIN group_student gs " +
            "               ON g.id = gs.group_id " +
            "       INNER JOIN students s\n" +
            "               ON gs.student_id = s.id " +
            "       INNER JOIN projects p\n" +
            "               ON s.id = p.student_id " +
            "       LEFT OUTER JOIN student_details sg " +
            "                    ON s.id = sg.student_id ")
    List<Group> getAllWithNativeQuery(); // Not working properly

}
