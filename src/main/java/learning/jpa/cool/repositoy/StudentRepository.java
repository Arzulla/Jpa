package learning.jpa.cool.repositoy;

import learning.jpa.cool.domain.Group;
import learning.jpa.cool.domain.Student;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Long> {

    @EntityGraph(attributePaths = { "projects" })
    @Query("SELECT s from Students s JOIN s.projects p " +
            "where s.id=:studentId")
     Student getALlWithEG(@Param("studentId") Long id);

    @EntityGraph(attributePaths = { "projects" })
    @Query("SELECT s from Students s JOIN s.projects p ")
     List<Student> getALlWithEG();
}
